package org.Zachar543.RedstoneProgrammer.Util;

import java.util.ArrayList;

import org.Zachar543.RedstoneProgrammer.RedstoneProgrammerPlugin;
import org.bukkit.entity.Player;
import org.bukkit.material.Sign;

public class Helper
{	
	public static int[] getSignDir(Sign sign)
	{
		return new int[0];
	}
	
	/**
	 * Gets the points on the line generated from the Bresenham Line Drawing Algorithm.
	 * @param x0 The start x position.
	 * @param y0 The start y position.
	 * @param x1 The end x position.
	 * @param y1 The end y position.
	 * @return The points on the line.
	 */
	public static ArrayList<int[]> getPointsOnLine(int x0, int y0, int x1, int y1)
	{
		return null;
	}
	
	
	/**
	 * <b>Internal: You should not need to call this!</b>
	 * @param p The RedstoneProgrammer plugin instance.
	 * @return boolean If the setup was successful.
	 */
	public static boolean setupEconomy(RedstoneProgrammerPlugin p)
    {
        return false;
    }
	
	/**
	 * Charges the player an amount of money.
	 * @param ply The player the charge.
	 * @param amount The amount to be charged.
	 * @return boolean If the player had enough money.
	 */
	public static boolean chargeMoney(Player ply, float amount)
	{
		return false;
	}
	/**
	 * Pays the player an amount of money.
	 * @param ply The player to pay.
	 * @param amount The amount to pay.
	 */
	public static void payMoney(Player ply, float amount) {}
	
	/**
	 * Converts total seconds into seconds, minutes, and hours. E.g. 2h 15m 59s
	 * @param seconds The total number of seconds.
	 * @return The string generated
	 */
	public static String secondsToString(int seconds)
	{
		return "";
	}
	
	/**
	 * Schedules a delayed run of a runnable instance.
	 * @param runnable The runnable to be run.
	 * @param delay The delay to wait.
	 */
	public static void timerDelay(Runnable runnable, long delay) {}
}
