package org.Zachar543.RedstoneProgrammer.CPUs;

import org.bukkit.block.Block;

public class CPUPortBlockLCD extends CPUPort
{
	public CPUPortBlockLCD(String portName, int width, int height)
	{
		super(portName, PortType.OUT_BLOCKLCD);
	}
	
	public int getWidth()
	{
		return 0;
	}
	public int getHeight()
	{
		return 0;
	}
	
	public Block getPixel(int x, int y)
	{
		return null;
	}
}
