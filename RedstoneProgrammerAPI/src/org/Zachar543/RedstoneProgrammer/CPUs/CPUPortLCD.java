package org.Zachar543.RedstoneProgrammer.CPUs;

public class CPUPortLCD extends CPUPort
{
	public CPUPortLCD(String portName, int width, int height)
	{
		super(portName, PortType.OUT_LCD);
	}
	
	public int getWidth()
	{
		return 0;
	}
	public int getHeight()
	{
		return 0;
	}
	
	public byte getPixel(int x, int y)
	{
		return 0;
	}
}
