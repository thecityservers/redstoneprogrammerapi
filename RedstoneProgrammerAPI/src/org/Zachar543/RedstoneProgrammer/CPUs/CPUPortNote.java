package org.Zachar543.RedstoneProgrammer.CPUs;

public class CPUPortNote extends CPUPort
{
	public CPUPortNote(String portName)
	{
		super(portName, PortType.OUT_NOTE);
	}
}
