package org.Zachar543.RedstoneProgrammer.CPUs;

public enum PortType
{	
	OUT_DIGITAL,
	OUT_ANALOG,
	OUT_CONSOLE,
	OUT_SIGN,
	IN_DIGITAL,
	IN_ANALOG,
	OUT_LCD,
	IN_CMD,
	IN_SIGN,
	OUT_NOTE,
	OUT_BLOCKLCD,
	OUT_BEDIT;
}
