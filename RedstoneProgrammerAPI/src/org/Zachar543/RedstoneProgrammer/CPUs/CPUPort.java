package org.Zachar543.RedstoneProgrammer.CPUs;

import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class CPUPort
{
	int ID = 0;
	String portName;
	PortType type = PortType.OUT_DIGITAL;
	Block block;
	CPU parent;
	
	public CPUPort(String portName) {}
	public CPUPort(String portName, PortType type) {}
	
	public String getName()
	{
		return "";
	}
	
	public int getID()
	{
		return 0;
	}
	
	public PortType getType()
	{
		return null;
	}
	
	public boolean isInput()
	{
		return false;
	}
	public boolean isOutput()
	{
		return false;
	}
	
	public boolean isAnalog()
	{
		return false;
	}
	public boolean isDigital()
	{
		return false;
	}
	public boolean isConsole()
	{
		return false;
	}
	public boolean isSign()
	{
		return false;
	}
	public boolean isLCD()
	{
		return false;
	}
	public boolean isBlockLCD()
	{
		return false;
	}
	public boolean isBlockEdit()
	{
		return false;
	}
	
	public Block getBlock()
	{
		return null;
	}

	public CPU getParent()
	{
		return null;
	}
	
	public void triggerDigitalInput(boolean active) {}
	public void triggerAnalogInput(byte value) {}
	public void triggerCommandInput(Player ply, String command, String[] args) {}
	public void triggerSignInput(Player ply) {}
	
	public void triggerDigitalOutput(boolean state) {}
	public void triggerAnalogOutput(byte value) {}
	public void triggerConsoleOutput(String message) {}
	public void triggerSignOutput(String message, int line) {}
	
	public void triggerLCDSetPixel(int x, int y, byte color) {}
	public void triggerLCDClear(int xoff, int yoff, int width, int height, byte color) {}
	public void triggerBLCDSetPixel(int x, int y, int ID, byte data) {}
	public void triggerBLCDClear(int xoff, int yoff, int width, int height, int ID, byte data) {}
	public void playNote(Instrument instrument, Note note) {}
	public void triggerBEditSetBlock(int x, int y, int z, int ID, byte data) {}
}
