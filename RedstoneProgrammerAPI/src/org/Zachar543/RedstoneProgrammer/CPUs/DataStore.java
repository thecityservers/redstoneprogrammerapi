package org.Zachar543.RedstoneProgrammer.CPUs;

import java.io.IOException;
import java.sql.SQLException;

public class DataStore
{
	public DataStore() {}

	public <T> void set(String id, T obj) {}
	
	public <T> T get(String id)
	{
		return null;
	}
	
	public void save(CPU cpu) throws IOException, SQLException {}
	public void load(CPU cpu) throws IOException, SQLException {}
}
