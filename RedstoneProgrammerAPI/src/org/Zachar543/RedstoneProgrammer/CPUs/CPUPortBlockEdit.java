package org.Zachar543.RedstoneProgrammer.CPUs;

public class CPUPortBlockEdit extends CPUPort
{
	public CPUPortBlockEdit(String portName)
	{
		super(portName, PortType.OUT_BEDIT);
	}
}
