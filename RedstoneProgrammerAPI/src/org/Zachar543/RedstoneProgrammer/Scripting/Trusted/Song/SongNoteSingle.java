package org.Zachar543.RedstoneProgrammer.Scripting.Trusted.Song;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;
import org.bukkit.Instrument;
import org.bukkit.Note;

public class SongNoteSingle extends SongNote
{
	public SongNoteSingle(float length, Instrument instrument, Note pitch)
	{
		super(length);
	}

	@Override
	public void play(RSPScript script, String portName) {}
}
