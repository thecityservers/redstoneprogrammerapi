package org.Zachar543.RedstoneProgrammer.Scripting.Trusted.Song;

import org.Zachar543.RedstoneProgrammer.RedstoneProgrammerPlugin;
import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class Song
{
	public Song(RedstoneProgrammerPlugin plugin, RSPScript script, String port, int tempo) {}

	public SongPart addPart()
	{
		return null;
	}
	public SongPart addPart(String suffix)
	{
		return null;
	}
	
	public void play() {}
	public void tick(int tempo) {}
	public void pause() {}
	public void unpause() {}
	public void stop() {}
	
	public boolean isPlaying()
	{
		return false;
	}
	public boolean isPaused()
	{
		return false;
	}
}
