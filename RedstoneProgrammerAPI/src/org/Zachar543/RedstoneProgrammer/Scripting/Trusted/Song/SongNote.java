package org.Zachar543.RedstoneProgrammer.Scripting.Trusted.Song;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public abstract class SongNote
{	
	public SongNote(float length) {}
	
	public abstract void play(RSPScript script, String portName);
}
