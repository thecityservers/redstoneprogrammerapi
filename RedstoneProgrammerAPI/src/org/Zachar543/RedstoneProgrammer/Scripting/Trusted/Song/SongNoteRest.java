package org.Zachar543.RedstoneProgrammer.Scripting.Trusted.Song;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class SongNoteRest extends SongNote
{
	public SongNoteRest(float length)
	{
		super(length);
	}
	
	@Override
	public void play(RSPScript script, String portName) {}
}
