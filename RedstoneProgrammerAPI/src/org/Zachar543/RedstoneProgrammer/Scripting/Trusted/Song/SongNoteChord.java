package org.Zachar543.RedstoneProgrammer.Scripting.Trusted.Song;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class SongNoteChord extends SongNote
{
	public SongNoteChord(int length, SongNoteSingle[] notes)
	{
		super(length);
	}

	@Override
	public void play(RSPScript script, String portName) {}
}
