package org.Zachar543.RedstoneProgrammer.Scripting.Trusted;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class SevenSegment
{
	static char[] suffixs = new char[] {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
	static char[][] intMaps = new char[][]
	{
		{'A', 'B', 'C', 'D', 'E', 'F'}, // 0
		{'B', 'C'}, // 1
		{'A', 'B', 'D', 'E', 'G'}, // 2
		{'A', 'B', 'C', 'D', 'G'}, // 3
		{'B', 'C', 'F', 'G'}, // 4
		{'A', 'C', 'D', 'F', 'G'}, // 5
		{'A', 'C', 'D', 'E', 'F', 'G'}, // 6
		{'A', 'B', 'C'}, // 7
		{'A', 'B', 'C', 'D', 'E', 'F', 'G'}, // 8
		{'A', 'B', 'C', 'D', 'F', 'G'}, // 9
	};
	
	/*
	 A 
	F B
	 G 
	E C
	 D 
	*/
	
	public SevenSegment(RSPScript script, String prefix) {}
	
	/**
	 * Clears the current display to off values.
	 */
	public void clear() {}
	/**
	 * Writes a number value to the screen.
	 * @param value A number from 0-9 that is to be written to the screen.
	 */
	public void setValue(int value) {}
}
