package org.Zachar543.RedstoneProgrammer.Scripting.Trusted;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class SevenSegmentArray
{
	static String[] decimalPrefixes = new String[] {".1", ".01", ".001"};
	static String[] prefixes = new String[] {"1", "10", "100", "1k", "10k", "100k", "1m", "10m", "100m", "1b"};
	
	public SevenSegmentArray(RSPScript script, String prefix, int numSegments, int decimals) {}
	
	/**
	 * Clears the all the associated displays to off values.
	 */
	public void clear() {}
	/**
	 * Writes a number with >= 1 characters to the screen.
	 * @param value A number to be written the the screen.
	 */
	public void setValue(float value) {}
}
