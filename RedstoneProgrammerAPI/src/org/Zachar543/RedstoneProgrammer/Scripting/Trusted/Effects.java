package org.Zachar543.RedstoneProgrammer.Scripting.Trusted;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;
import org.bukkit.Effect;
import org.bukkit.Location;

public class Effects
{
	/**
	 * Plays an effect at the specified location.
	 * @param script The script the call is from.
	 * @param pos The position to play the effect.
	 * @param effect The effect to play.
	 * @param data The data from the effect.
	 */
	public static void playEffect(RSPScript script, Location pos, Effect effect, int data) {}
	/**
	 * Creates an explosion effect at the specified location.
	 * @param script The script the call is from.
	 * @param pos The position to make the explosion.
	 */
	public static void explode(RSPScript script, Location pos) {}
	/**
	 * Strikes lighting at the specified location.
	 * @param script The script the call is from.
	 * @param pos The position to strike with lightning.
	 */
	public static void lightning(RSPScript script, Location pos) {}
}
