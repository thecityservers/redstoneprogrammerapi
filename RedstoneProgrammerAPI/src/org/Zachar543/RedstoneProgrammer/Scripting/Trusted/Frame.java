package org.Zachar543.RedstoneProgrammer.Scripting.Trusted;

import org.Zachar543.RedstoneProgrammer.Scripting.RSPScript;

public class Frame
{
	public Frame(int width, int height) {}
	public Frame(int[] data, int length, int height) {}
	
	/**
	 * Writes the frame's buffer to the LCD.
	 * @param script The script the port belongs to.
	 * @param port The port to be written.
	 */
	public void apply(RSPScript script, String port) {}
	/**
	 * Captures the current LCD's data in the frame's buffer.
	 * @param script The script the port belongs to.
	 * @param port The port to be read.
	 */
	public void capture(RSPScript script, String port) {}
}
