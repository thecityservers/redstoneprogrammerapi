package org.Zachar543.RedstoneProgrammer.Scripting;

import java.util.ArrayList;

import org.Zachar543.RedstoneProgrammer.RedstoneProgrammerPlugin;
import org.Zachar543.RedstoneProgrammer.CPUs.CPU;
import org.Zachar543.RedstoneProgrammer.CPUs.CPUPort;
import org.Zachar543.RedstoneProgrammer.CPUs.PortType;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

/**
 * @author Zachar543
 */
public class RSPScript
{
	public static byte white = 0;
	public static byte orange = 1;
	public static byte purple = 2;
	public static byte lightblue = 3;
	public static byte yellow = 4;
	public static byte green = 5;
	public static byte pink = 6;
	public static byte darkgray = 7;
	public static byte gray = 8;
	public static byte cyan = 9;
	public static byte darkpurple = 10;
	public static byte blue = 11;
	public static byte brown = 12;
	public static byte darkgreen = 13;
	public static byte red = 14;
	public static byte black = 15;
	
	/**
	 * The base class for a script that is loaded into the RSP VM.
	 * @param plugin
	 */
	public RSPScript(RedstoneProgrammerPlugin plugin) {}
	
	/**
	 * Gets the CPU that is running this script instance.
	 * @return CPU The CPU running this script instance.
	 */
	public CPU getCPU()
	{
		return null;
	}
	void setCPU(CPU cpu) {}
	
	public RedstoneProgrammerPlugin getPlugin()
	{
		return null;
	}
	
	/**
	 * Initializes a port with the specified name. This <b>MUST</b> be called in <i>onInit()</i>!
	 * @param portname The name of the port to be initialized.
	 * @param type The PortType type of the port.
	 */
	public void initPort(String portname, PortType type) {}
	
	/**
	 * Writes the value to the specified digital port.
	 * @PortType Digital Output Port
	 * @param portname The port's name.
	 * @param state The value to write (On/Off).
	 */
	public void digitalWrite(String portname, boolean state) {}
	
	/**
	 * Writes the value to the specified analog port.
	 * @PortType Analog Output Port
	 * @param portname The port's name.
	 * @param value The value to write (0-15).
	 */
	public void analogWrite(String portname, byte value) {}
	
	/**
	 * Prints a message to the console.
	 * @param message The message to be printed.
	 */
	public void print(String message) {}
	
	/**
	 * Writes a message on the specified sign output to the specified line.
	 * @PortType Sign Output Port
	 * @param portname The port to be written to.
	 * @param message The message to be written (< 24 characters!)
	 * @param line The line the message will be written on (0-3).
	 */
	public void signSetLine(String portname, String message, int line) {}
	
	/**
	 * Sets the pixel on the specified LCD at the x and y position.
	 * @PortType LCD Port
	 * @param portname The LCD port to be written to.
	 * @param x The LCD x position.
	 * @param y The LCD y position.
	 * @param color The color to be written (0-15: Wool Data Values).
	 */
	public void lcdSetPixel(String portname, int x, int y, byte color) {}
	
	/**
	 * Gets the pixel of the LCD at the x, y coordinates.
	 * @PortType LCD Port
	 * @param portname The LCD to be read.
	 * @param x The LCD x position.
	 * @param y The LCD y position.
	 * @return The color at the coordinates (0-15: Wool Data Values).
	 */
	public byte lcdGetPixel(String portname, int x, int y)
	{
		return 0;
	}
	
	/**
	 * Clears the screen starting at x, y and extending width, height to the specified color.
	 * @PortType LCD Port
	 * @param portname The LCD to be written to.
	 * @param x The LCD x position.
	 * @param y The LCD y position.
	 * @param width The distance from x to be set.
	 * @param height The distance from y to be set.
	 * @param color The color to clear to (0-15: Wool Data Values).
	 */
	public void lcdClear(String portname, int x, int y, int width, int height, byte color) {}
	
	/**
	 * Sets the pixel on the specified BlockLCD at the x and y position.
	 * @PortType BlockLCD Port
	 * @param portname The BlockLCD port to be written to.
	 * @param x The BlockLCD x position.
	 * @param y The BlockLCD y position.
	 * @param ID The block ID to be written.
	 * @param data The block data to be written.
	 */
	public void blcdSetPixel(String portname, int x, int y, int ID, byte data) {}
	
	/**
	 * Sets the pixel on the specified BlockLCD at the x and y position.
	 * @PortType BlockLCD Port
	 * @param portname The BlockLCD port to be written to.
	 * @param x The BlockLCD x position.
	 * @param y The BlockLCD y position.
	 * @param ID The block ID to be written.
	 * @param data The block data to be written.
	 * @param physics Whether or not to trigger block updates.
	 */
	public void beditSetBlock(String portname, int x, int y, int z, int ID, byte data, boolean physics) {}
	
	/**
	 * Clears the screen starting at x, y and extending width, height to the specified ID and data.
	 * @PortType BlockLCD Port
	 * @param portname The BlockLCD to be written to.
	 * @param x The BlockLCD x position.
	 * @param y The BlockLCD y position.
	 * @param width The distance from x to be set.
	 * @param height The distance from y to be set.
	 * @param ID The block ID to clear to.
	 * @param data The block data to clear to.
	 */
	public void blcdClear(String portname, int x, int y, int width, int height, int ID, byte data) {}
	
	/**
	 * Plays a <i>Note Block</i> note from the port.
	 * @PortType Note Port
	 * @param portname The Port to play sound note from.
	 * @param instrument The instrument to play.
	 * @param note The note to play.
	 */
	public void notePlay(String portname, Instrument instrument, Note note) {}
	
	/**
	 * Sets the block at the x, y, z coordinates to the ID and data values.
	 * @PortType BlockEdit Port
	 * @param portname The port name to set relative to.
	 * @param x The x coordinate to set.
	 * @param y The y coordinate to set.
	 * @param z The z coordinate to set.
	 * @param ID The block ID to set.
	 * @param data The block data to set.
	 */
	public void beditSetBlock(String portname, int x, int y, int z, int ID, byte data) {}
	
	/**
	 * Gets the Block object at the x, y, z coordinates.
	 * @param portname
	 * @PortType BlockEdit Port
	 * @param x The x coordinate to get.
	 * @param y The y coordinate to get.
	 * @param z The z coordinate to get.
	 * @return Block The block at the x, y, z coordinates relative to the port.
	 */
	public Block beditGetBlock(String portname, int x, int y, int z)
	{
		return null;
	}
	
	/**
	 * Gets a persistent data values with the specified key.
	 * @param key The key value of the data to get.
	 * @return T The persistent object with the specified key.
	 */
	public <T> T getDataValue(String key)
	{
		return null;
	}
	
	/**
	 * Sets a persistent data value.
	 * @param key The key to be used for retrieval.
	 * @param value The value to be set.
	 */
	public <T> void setDataValue(String key, T value) {}
	
	/**
	 * This is called when the CPU first initializes. This is where you should initialize your ports.
	 * @type Event
	 */
	public void onInit() {}
	/**
	 * This is called when the port receives a change in Redstone current.
	 * @type Event
	 * @param port The port receiving the change.
	 * @param state The value of the Redstone current.
	 */
	public void onDigitalInput(String port, boolean state) {}
	/**
	 * This is called when the port receives a change in Redstone current.
	 * @type Event
	 * @param port The port receiving the change.
	 * @param value The value of the Redstone current.
	 */
	public void onAnalogInput(String port, byte value) {}
	/**
	 * This is called when the port detects that the play typed a command.
	 * @type Event
	 * @param port The port receiving the event.
	 * @param ply The player that sent the command.
	 * @param command The command that was run.
	 * @param args The arguments the player sent (args[0] is the command that was run).
	 */
	public void onCommandRecieved(String port, Player ply, String command, String[] args) {}
	/**
	 * This is called when a player right clicks on the sign input.
	 * @type Event
	 * @param port The port receiving the event.
	 * @param ply The player that right clicked the sign input.
	 */
	public void onSignInput(String port, Player ply) {}
	/**
	 * This is called 20 times per second (each tick of the Minecraft server).
	 * @type Event
	 */
	public void onTick() {}
	/**
	 * This is called when a player dies.
	 * @type Event
	 * @param ply The player that died.
	 */
	public void onPlayerDeath(Player ply) {}
	/**
	 * This is called when the player clicks the respawn button after dieing.
	 * @type Event
	 * @param ply The player that respawned.
	 */
	public void onPlayerRespawn(Player ply) {}
	/**
	 * This is called when the player disconnects from the server.
	 * @type Event
	 * @param ply The player that disconnected.
	 */
	public void onPlayerQuit(Player ply) {}
	/**
	 * This is called when the power supplied to the CPU is toggled. E.g. The power is turned off.
	 * @type Event
	 * @param state The new on state of the CPU.
	 */
	public void onPowerStateChange(boolean state) {}
}
