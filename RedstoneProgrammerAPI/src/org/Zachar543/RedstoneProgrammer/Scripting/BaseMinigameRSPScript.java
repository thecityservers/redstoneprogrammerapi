package org.Zachar543.RedstoneProgrammer.Scripting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.Zachar543.RedstoneProgrammer.RedstoneProgrammerPlugin;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public abstract class BaseMinigameRSPScript extends RSPScript
{
	public BaseMinigameRSPScript(RedstoneProgrammerPlugin plugin)
	{
		super(plugin);
		rand = new Random();
	}
	
	protected String name = "Base Minigame";
	
	protected float entryFee;
	protected int minRequiredPlayers;
	protected int maxPlayers;
	protected int signupPeriodDelay;
	protected int votingPeriodDelay;
	protected int entryPeriodDelay;
	
	protected int startupTimer;
	protected int time;
	protected long ticks;
	protected int numEntries;
	protected int numVotes;
	protected int jackpot;
	
	protected int xSize;
	protected int ySize;
	protected int zSize;
	protected Random rand;
	
	protected Status status;
	
	protected ArrayList<Player> players;
	
	protected ArrayList<Player> votedPlayers;
	protected HashMap<Integer, Integer> votes;
	
	protected ArrayList<Player> enteredPlayers;
	HashMap<String, Location> lastPos;
	HashMap<String, Location> toReturnBacklog;
	
	protected ArrayList<Player> alivePlayers;
	protected ArrayList<Player> deadPlayers;
	
	protected ArrayList<Player> tmpAlivePlayers;
	
	enum Status
	{
		WAITING_FOR_PLAYERS,
		SIGNUP_PERIOD,
		VOTING_PERIOD,
		ENTRY,
		IN_PROGRESS;
	}
	
	@Override
	public void onInit() {}
	
	@Override
	public void onCommandRecieved(String port, Player ply, String command, String[] args) {}
	
	@Override
	public void onSignInput(String port, Player ply) {}
	
	@Override
	public void onPlayerDeath(Player ply) {}
	
	@Override
	public void onPlayerQuit(Player ply) {}
	@Override
	public void onPlayerRespawn(final Player ply) {}
	
	@Override
	public void onTick() {}

	@Override
	public void onPowerStateChange(boolean state) {}
	
	
	protected abstract void displayVotingInstructions(Player ply);
	protected abstract void displayVotingOptions(Player ply);
	protected abstract void tallyVotes();
	protected abstract boolean isValidVote(Player ply, int vote);
	protected abstract void resetMap();
	protected abstract void onRoundEnd();
	protected abstract void clearVariables();
	protected abstract void onInProgressTick();
	protected abstract Location getEntryPosition();
	
	
	void attemptPlayerTeleport(Player ply, Location pos) {}
	
	protected void rewardPlayer(Player ply) {}
	
	void enterVote(Player ply, int vote) {}
	
	protected void endRound() {}
	
	protected void setStatus(Status status) {}
	void updateStatusSigns() {}
	
	protected void addPlayer(Player ply) {}
	protected void removePlayer(Player ply) {}
	
	ArrayList<Player> tmpPlayers = new ArrayList<Player>();
	void redrawPlayerSigns() {}
	
	protected boolean isPlayerInPlay(Player ply)
	{
		return false;
	}
}
